# MusicLearn #

This is the web platform where people can learn how to play piano.
The current project result and status can be observed on the stend: http://inno-ijl.ru/music-learn/my-courses

To run project do the following steps:
```
npm install
npm start
```

## Design Layouts ##
https://www.figma.com/file/82iw0bNHKEGw9HONbVq6Y5/music-learn?node-id=0%3A1

## MVP1 ##

### Frontend Structure ###
* Authorization page
    + Sign in
    + Sign up
* Main page:
    + Navigation
    + Title
    + Description
    + Contacts
* Video courses:
    + List of courses:
        + List of videos
* Profile
    + Settings
    + My courses

## MVP2 ##
* Courses Functional:
    + Video description with resources
    + Add/Delete course
    + Edit course
        + Edit information
        + Add/Delete video
* Fortepiano Notes Functional:
    + List of notes
    + Add/Delete notes
* Payment page
* Profile:
    + My notes
