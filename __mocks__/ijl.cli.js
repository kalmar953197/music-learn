const {navigations, features, config,} = require('../ijl.config');

module.exports = {
    getConfigValue : (key) => config[key],
    getNavigations: () => {
        return {...navigations,};
    },
};