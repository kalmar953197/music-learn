module.exports = {
    Switch: () => ('Switch'),
    Route: () => ('Route'),
    BrowserRouter: () => ('BrowserRouter'),
    Redirect: () => ('Redirect'),
};