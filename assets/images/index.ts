import CourseIMG_rus from "./courses.jpg";
import CourseIMG_eng from "./courses2.jpg";
import CourseIMG_alt from "./courses3.png";
import UserAvatar from "./default-avatar.jpg";
import NotesIMG from "./notes.jpg";
import AuthorPic from "./teacher.png";

export {
  CourseIMG_rus,
  CourseIMG_eng,
  CourseIMG_alt,
  UserAvatar,
  NotesIMG,
  AuthorPic,
};
