const path = require("path");
const pkg = require("./package");
const postCssSettings = require("./postcss.config.js");
const isProd = process.env.NODE_ENV === "production";

module.exports = {
  apiPath: "stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
    },
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: [
            {
              loader: "style-loader",
            },
            {
              loader: "css-loader",
              options: {
                modules: {
                  mode: "local",
                  exportGlobals: true,
                  localIdentName: isProd
                    ? "[hash:base64]"
                    : "[path]--[name]__[local]--[hash:base64:3]",
                  localIdentContext: path.resolve(__dirname, "src"),
                  exportLocalsConvention: "camelCase",
                },
              },
            },
            {
              loader: "postcss-loader",
              options: { postcssOptions: { postCssSettings } },
            },
          ],
        },
      ],
    },
  },
  navigations: {
    "music-learn": "/music-learn",
    "music-learn.login": "/music-learn/login",
    "music-learn.register": "/music-learn/register",
    "music-learn.my-courses": "/music-learn/my-courses",
    "music-learn.courses": "/music-learn/courses",
    "music-learn.contacts": "/music-learn/contacts",
  },
  config: {
    "music-learn.api": "/api/",
  },
};
