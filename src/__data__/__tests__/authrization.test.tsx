import { describe, it, expect } from "@jest/globals";
import {
  initialState,
  authorizationActions,
  authorizationSelectors,
  authorizationReducer,
} from "../features/authorization/authorizationSlice";

describe("authorization", () => {
  describe("log in", () => {
    it("executes login correctly", () => {
      const nextState = {
        isAuthorized: true,
        id: 1,
        email: "email",
        login: "login",
        password: "password",
      };
      const result = authorizationReducer(
        initialState,
        authorizationActions.login({
          id: 1,
          email: "email",
          login: "login",
          password: "password",
        })
      );
      expect(result).toEqual(nextState);
    });
  });
});

describe("authorization", () => {
  describe("log out", () => {
    it("executes logout correctly", () => {
      const currentState = {
        isAuthorized: true,
        id: 1,
        email: "email",
        login: "login",
        password: "password",
      };
      const nextState = {
        isAuthorized: false,
        id: 0,
        email: null,
        login: null,
        password: null,
      };
      const result = authorizationReducer(
        currentState,
        authorizationActions.logout()
      );
      expect(result).toEqual(nextState);
    });
  });
});

describe("authorization", () => {
  describe("register", () => {
    it("executes register correctly", () => {
      const nextState = {
        isAuthorized: true,
        id: 1,
        email: "email",
        login: "login",
        password: "password",
      };
      const result = authorizationReducer(
        initialState,
        authorizationActions.register({
          id: 1,
          email: "email",
          login: "login",
          password: "password",
        })
      );
      expect(result).toEqual(nextState);
    });
  });
});
