import * as React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import store from "../store";

describe("store", () => {
  it("rendering", () => {
    const wrapper = store;

    expect(wrapper).toMatchSnapshot();
  });
});
