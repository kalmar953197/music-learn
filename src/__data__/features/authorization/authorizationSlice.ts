import { createSlice, createSelector } from "@reduxjs/toolkit";

import { rootSelector } from "../../root-selector";

export type AuthorizationState = {
  isAuthorized: boolean;

  id: number;

  email: string;

  login: string;

  password: string;
};

export const initialState: AuthorizationState = {
  isAuthorized: false,

  id: 0,

  email: null,

  login: null,

  password: null,
};

export const authorizationSlice = createSlice({
  name: "authorization",

  initialState,

  reducers: {
    login(state, { payload: { id, email, login, password } }) {
      state.isAuthorized = true;

      state.id = id;

      state.email = email;

      state.login = login;

      state.password = password;
    },

    logout(state) {
      state.isAuthorized = false;

      state.id = 0;

      state.email = null;

      state.login = null;

      state.password = null;
    },

    register(state, { payload: { id, email, login, password } }) {
      state.isAuthorized = true;

      state.id = id;

      state.email = email;

      state.login = login;

      state.password = password;
    },
  },
});

export const { login, logout, register } = authorizationSlice.actions;

const authorizationSelector = createSelector(
  rootSelector,

  (state) => state.authorization
);

const authorizationSelectors = {
  isAuthorized: createSelector(
    authorizationSelector,

    (state) => state.isAuthorized
  ),
};

const {
  actions: authorizationActions,

  reducer: authorizationReducer,
} = authorizationSlice;

export { authorizationActions, authorizationReducer, authorizationSelectors };
