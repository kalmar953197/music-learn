import { configureStore, combineReducers } from "@reduxjs/toolkit";

import { authorizationReducer } from "./features/authorization/authorizationSlice";


const store = configureStore({

  reducer: combineReducers({

    authorization: authorizationReducer,

  }),

});


export type RootState = ReturnType<typeof store.getState>;

export default store;
