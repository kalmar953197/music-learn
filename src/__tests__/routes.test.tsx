import * as React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import { useRoutes } from "../routes";

describe("useRoutes(false)", () => {
  it("rendering with false", () => {
    const wrapper = mount(useRoutes(false));

    expect(wrapper).toMatchSnapshot();
  });
});

describe("useRoutes(false)", () => {
  it("rendering with true", () => {
    const wrapper = mount(useRoutes(true));

    expect(wrapper).toMatchSnapshot();
  });
});
