import React from "react";
import { BrowserRouter } from "react-router-dom";

import { useRoutes } from "./routes";
import store from "./__data__/store";
import { Provider } from "react-redux";

const App = (): BrowserRouter => {
  const routes = useRoutes(false); //switch to false
  return (
    <Provider store={store}>
      <BrowserRouter>{routes}</BrowserRouter>
    </Provider>
  );
};

export default App;
