import React from "react";

import style from "./Button.css";
import ProfileButton from "../ProfileButton/ProfileButton";

interface Props {
  color: string;
  width: string;
  height: string;
  text?: React.ReactNode;
  href: string;
  type: string;
}

const Button: React.FC<Props> = ({
  color,
  width,
  height,
  href,
  text,
  type,
}) => {
  if (color == "pink") {
    switch (type) {
      case "focused":
        return (
          <a href={href}>
            <button
              style={{ height, width }}
              className={style.focused_pink_button}
              type="button"
            >
              {text}
            </button>
          </a>
        );
      case "underlined":
        return (
          <a href={href}>
            <button
              style={{ height, width }}
              className={style.underlined_pink_button}
              type="button"
            >
              {text}
            </button>
          </a>
        );
      case "filled":
        return (
          <a href={href}>
            <button
              style={{ height, width }}
              className={style.filled_pink_button}
              type="button"
            >
              {text}
            </button>
          </a>
        );
      case "simple":
        return (
          <a href={href}>
            <button
              style={{ height, width }}
              className={style.pink_button}
              type="button"
            >
              {text}
            </button>
          </a>
        );
      default:
        return <button type="button">{text}</button>;
    }
  }
  switch (type) {
    case "focused":
      return (
        <a href={href}>
          <button
            style={{ height, width }}
            className={style.focused_white_button}
            type="button"
          >
            {text}
          </button>
        </a>
      );
    case "underlined":
      return (
        <a href={href}>
          <button
            style={{ height, width }}
            className={style.underlined_white_button}
            type="button"
          >
            {text}
          </button>
        </a>
      );
    case "simple":
      return (
        <a href={href}>
          <button
            style={{ height, width }}
            className={style.white_button}
            type="button"
          >
            {text}
          </button>
        </a>
      );
    default:
      return <button type="button">{text}</button>;
  }
};
export default Button;
