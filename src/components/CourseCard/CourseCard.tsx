import React from "react";

import style from "./CourseCard.css";

import { ArrowRight } from "react-feather";

import PropTypes from 'prop-types';

interface Props {
  is_category: boolean;
  text: string;
  description?: string;
  href: string;
  image: string;
}

const CourseCard: React.FC<Props> = ({
  href,
  text,
  image,
  is_category,
  description,
}) => {
  if (is_category) {
    return (
      <a href={href} className={style.category_link}>
        <div className={style.course}>
          <div className={style.img_container}>
            <img src={image} />
            <div className={style.glassblur}></div>
            <div className={style.arrow_btn}>
              <ArrowRight color="black" size={35} />
            </div>
          </div>
          <p className={style.name}>{text}</p>
          <p className={style.description_block}>{description}</p>
        </div>
      </a>
    );
  }
  return (
    <a href={href} className={style.course_link}>
      <div className={style.course}>
        <div className={style.img_container}>
          <img src={image} />
          <div className={style.glassblur}></div>
          <div className={style.arrow_btn}>
            <ArrowRight color="black" size={35} />
          </div>
        </div>
        <p>{text}</p>
      </div>
    </a>
  );
};

CourseCard.propTypes = {
  is_category: PropTypes.bool,
  text: PropTypes.string,
  description: PropTypes.string,
  href: PropTypes.string,
  image: PropTypes.string,
};

export default CourseCard;
