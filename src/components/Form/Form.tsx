import React from 'react';

import style from './Form.css'

import PropTypes from "prop-types";

interface Props {
	icon: JSX.Element;
	placeholder: string;
	type: string;
}

const Form: React.FC<Props> = ({icon, placeholder, type}) => {
	return (
		<div className={style.FormWrapper}>
			<i className={style.Icon}>{icon}</i>
			<label className={style.FormLabel} htmlFor={placeholder}>{placeholder}</label>
			<input
				className={style.Form}
				type={type}
				name={placeholder}
				id={placeholder}
				placeholder={placeholder}
				autoComplete="off"
			/>
		</div>
	)
}

Form.propTypes = {
	icon: PropTypes.any,
	placeholder: PropTypes.string,
	type: PropTypes.string,
};

export default Form
