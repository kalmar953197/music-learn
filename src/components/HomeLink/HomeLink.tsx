import React from "react";

import style from "./HomeLink.css";

import { Home } from "react-feather";

const HomeLink: React.FC = () => {
  return (
    <React.Fragment>
      <a href="/music-learn" className={style.HomeLink}>
        <i className={style.HomeIcon}>
          <Home color="white" size={35} />
        </i>
        <h2 className={style.HomeText}>Home</h2>
      </a>
    </React.Fragment>
  );
};

export default HomeLink;
