import React from 'react';

import style from './MainTitle.css'

import { Activity } from 'react-feather';


const MainTitle: React.FC = () => {
	return (
		<React.Fragment>
			<i><Activity className={style.ActivityIcon} color="white" size={60}/></i>
			<h1 className={style.TitleText}>MusicLearn</h1>
		</React.Fragment>
	)
}

export default MainTitle
