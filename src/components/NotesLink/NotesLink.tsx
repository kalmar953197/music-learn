import React from 'react';

import style from './NotesLink.css'

const NotesLink: React.FC = () => {
    return (
        <div>
            <a href="/" className={style.NotesLink}>
                <h2 className={style.NotesLinkText}>Notes</h2>
            </a>
        </div>
    )
}

export default NotesLink
