import React from "react";

import { User } from "react-feather";
import style from "./ProfileButton.css";
import Button from "../Button/Button";

import PropTypes from "prop-types";

interface Props {
  isAuthorized: boolean;
}

const ProfileButton: React.FC<Props> = ({ isAuthorized }) => {
  if (isAuthorized) {
    return (
      <div className={style.profile_dropdown}>
        <a href="/music-learn/my-courses" className={style.icon_button}>
          <User size={35} />
        </a>
      </div>
    );
  }
  return (
    <div className={style.profile_dropdown}>
      <a href="/music-learn/login" className={style.icon_button}>
        <User size={35} />
      </a>
      <div className={style.dropdown_content}>
        <Button
          color="white"
          width="130px"
          height="auto"
          href="/music-learn/login"
          text ="Sign In"
          type="focused"
        />
        <Button
          color="pink"
          width="130px"
          height="auto"
          href="/music-learn/register"
          text ="Sign Up"
          type="filled"
        />
      </div>
    </div>
  );
};

ProfileButton.propTypes = {
    isAuthorized: PropTypes.bool,
};

export default ProfileButton;
