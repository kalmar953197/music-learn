import * as React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import Button from "../Button/Button";

describe("<Button />", () => {
  it("renders pink simple button", () => {
    const wrapper = mount(
      <Button
        color="pink"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="simple"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  it("renders pink focused button", () => {
    const wrapper = mount(
      <Button
        color="pink"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="focused"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  it("renders pink underlined button", () => {
    const wrapper = mount(
      <Button
        color="pink"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="underlined"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  it("renders pink filled button", () => {
    const wrapper = mount(
      <Button
        color="pink"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="filled"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  it("renders pink button of unknown type", () => {
    const wrapper = mount(
      <Button
        color="pink"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="unknown type"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  it("renders white simple button", () => {
    const wrapper = mount(
      <Button
        color="white"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="simple"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  it("renders white focused button", () => {
    const wrapper = mount(
      <Button
        color="white"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="focused"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  it("renders white underlined button", () => {
    const wrapper = mount(
      <Button
        color="white"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="underlined"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  it("renders white button of unknown type", () => {
    const wrapper = mount(
      <Button
        color="white"
        width="250px"
        height="65px"
        href="/"
        text="Text"
        type="unknown type"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
