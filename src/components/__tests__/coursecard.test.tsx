import * as React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import CourseCard from "../CourseCard/CourseCard";

describe("<CourseCard />", () => {
  it("renders card", () => {
    const wrapper = mount(
      <CourseCard
        key={16}
        is_category={false}
        href="/"
        text="Course"
        image=""
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it("renders category", () => {
    const wrapper = mount(
      <CourseCard key={16} is_category={true} href="/" text="Course" image="" />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
