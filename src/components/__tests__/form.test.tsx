import * as React from 'react'
import {mount} from 'enzyme';

import {describe, it, expect} from '@jest/globals'

import Form from '../Form/Form';
import {Mail, User, Lock} from "react-feather";


describe('<Form />', () => {
    it('rendering mail', () => {
        const wrapper = mount(<Form icon={<Mail color="white" size={45}/>}
                                    placeholder={"placeholder_test"} type={"type_test"}/>);

        expect(wrapper).toMatchSnapshot();
    });
});

describe('<Form />', () => {
    it('rendering user', () => {
        const wrapper = mount(<Form icon={<User color="white" size={45}/>}
                                    placeholder={"placeholder_test"} type={"type_test"}/>);

        expect(wrapper).toMatchSnapshot();
    });
});

describe('<Form />', () => {
    it('rendering lock', () => {
        const wrapper = mount(<Form icon={<Lock color="white" size={45}/>}
                                    placeholder={"placeholder_test"} type={"type_test"}/>);

        expect(wrapper).toMatchSnapshot();
    });
});
