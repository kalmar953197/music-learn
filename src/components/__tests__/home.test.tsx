import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'

import HomeLink from '../HomeLink/HomeLink';


describe('<HomeLink />', () => {
    it('rendering', () => {
        const wrapper = mount(<HomeLink />);

        expect(wrapper).toMatchSnapshot();
    });
});