import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'

import MainTitle from '../MainTitle/MainTitle';


describe('<MainTitle />', () => {
    it('rendering', () => {
        const wrapper = mount(<MainTitle  />);

        expect(wrapper).toMatchSnapshot();
    });
});
