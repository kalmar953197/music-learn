import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'

import NotesLink from '../NotesLink/NotesLink';


describe('<NotesLink />', () => {
    it('rendering', () => {
        const wrapper = mount(<NotesLink  />);

        expect(wrapper).toMatchSnapshot();
    });
});
