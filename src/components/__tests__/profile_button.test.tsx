import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'

import ProfileButton from '../ProfileButton/ProfileButton';


describe('<ProfileButton />', () => {
    it('rendering auth', () => {
        const wrapper = mount(<ProfileButton isAuthorized={true}  />);

        expect(wrapper).toMatchSnapshot();
    });
});

describe('<ProfileButton />', () => {
    it('rendering no-auth', () => {
        const wrapper = mount(<ProfileButton isAuthorized={false}  />);

        expect(wrapper).toMatchSnapshot();
    });
});