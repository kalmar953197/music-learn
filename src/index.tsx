import React from "react";
import ReactDom from "react-dom";

import App from "./app";

import store from "./__data__/store";
import { Provider } from "react-redux";

export default function main(): JSX.Element {
  return <App />;
}

export const mount = async (Component): Promise<void> => {
  ReactDom.render(<Component />, document.getElementById("app"));

    if (module.hot) {

        module.hot.accept("./app", () => {

            ReactDom.render(<App />, document.getElementById("app"));

        });

    }

};

export const unmount = (): void => {
  ReactDom.unmountComponentAtNode(document.getElementById("app"));
};
