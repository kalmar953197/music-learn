import React from "react";

import style from "./AuthPage.css";

import HomeLink from "../../components/HomeLink/HomeLink";
import MainTitle from "../../components/MainTitle/MainTitle";

import PropTypes from "prop-types";

interface Props {
    children?: React.ReactNode;
}

const AuthPage: React.FC<Props> = ({children}) => {
  return (
    <div className={style.GridLayout}>
      <div className={style.HomeLink}>
        <HomeLink />
      </div>
      <div className={style.MainTitle} tabIndex={0} role="banner">
        <MainTitle />
      </div>
      <main className={style.SignIn}>
        <div>{children}</div>
      </main>
    </div>
  );
};

AuthPage.propTypes = {
    children: PropTypes.any,
};

export default AuthPage;
