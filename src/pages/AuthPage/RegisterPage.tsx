import React, { Component, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Form from "../../components/Form/Form";
import AuthPage from "./AuthPage";

import { useForm } from "react-hook-form";
import style from "./SignRegister.css";

import { Mail, User, Lock } from "react-feather";
import { useSelector, useDispatch } from "react-redux";
import {
  authorizationSelectors,
  register,
} from "../../__data__/features/authorization/authorizationSlice";
import { URLs } from "../../urls";

function RegisterPage(): JSX.Element {
  const auth_data = useSelector(authorizationSelectors.isAuthorized);
  let history = useHistory();

  useEffect(() => {
    if (auth_data) {
      history.push(URLs.home.url);
    }
  });

  const dispatch = useDispatch();

  const { handleSubmit } = useForm({ mode: "onChange" });
  function loginHandler() {
    dispatch(register({ id: 0, email: null, login: null, password: null }));
    console.log(auth_data);
  }

  return (
    <AuthPage>
      <h3 className={style.FormHeaderText} tabIndex={0}>
        Register
      </h3>
      <form onSubmit={handleSubmit(loginHandler)}>
        <Form
          placeholder="Email address"
          type="text"
          icon={<Mail className={style.EmailIcon} color="white" size={45} />}
        />
        <Form
          placeholder="Nickname"
          type="text"
          icon={<User className={style.UserIcon} color="white" size={45} />}
        />
        <Form
          placeholder="Password"
          type="password"
          icon={<Lock className={style.LockIcon} color="white" size={45} />}
        />
        <button className={style.SendButton} type="submit">
          Register
        </button>
      </form>
    </AuthPage>
  );
}

export default RegisterPage;
