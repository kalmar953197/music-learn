import React, { Component } from "react";
import style from "./ContactsPage.css";

import HomeLink from "../../components/HomeLink/HomeLink";
import MainTitle from "../../components/MainTitle/MainTitle";
import ProfileButton from "../../components/ProfileButton/ProfileButton";

import {
  GitHub,
  Figma,
  Instagram,
  Facebook,
  Youtube,
  Mail,
  Code,
} from "react-feather";

import Button from "../../components/Button/Button";
import { useSelector } from "react-redux";
import { authorizationSelectors } from "../../__data__/features/authorization/authorizationSlice";

function ContactsPage(): JSX.Element {
  const is_auth = useSelector(authorizationSelectors.isAuthorized);
  console.log(is_auth);
  return (
    <div className={style.body}>
      <header className={style.header}>
        <div className={style.navigation}>
          <HomeLink />
          <div className={style.links}>
            <Button
              color="white"
              width="auto"
              height="auto"
              href="/music-learn/courses"
              text="Courses"
              type="underlined"
            />
            <Button
              color="white"
              width="auto"
              height="auto"
              href="/"
              text="Notes"
              type="underlined"
            />
            <Button
              color="white"
              width="auto"
              height="auto"
              href="/music-learn/contacts"
              text="Contacts"
              type="underlined"
            />
            <ProfileButton isAuthorized={is_auth} />
          </div>
        </div>
        <div role="banner">
          <MainTitle />
        </div>
      </header>
      <main className={style.main}>
        <div>
          <div>
            <h1>About</h1>
            <p>
              MusicLearn is a web platform where people can learn how to play
              piano.{" "}
            </p>
            <p>
              The project was developed by MusicLearn team for Enterprise
              programming on JavaScript - Advanced course in Innopolis
              University (Spring semester 2021). Our team is presented by Anna
              Gorb, Marina Ivanova and Yulia Chukanova.
            </p>
          </div>
          <div>
            <h1>Resources</h1>
            <ul>
              <li>
                <Code size={18} /> Code can be observed in the{" "}
                <a href="https://bitbucket.org/kalmar953197/music-learn/src/master/">
                  Bitbucket repo
                </a>
              </li>
              <li>
                <Figma size={18} /> Design layouts are available on{" "}
                <a href="https://www.figma.com/file/82iw0bNHKEGw9HONbVq6Y5/music-learn?node-id=0%3A1">
                  Figma
                </a>
              </li>
              <li>
                <Youtube size={18} /> You can also check playlist with free
                courses on <a href="https://www.youtube.com">Youtube channel</a>
              </li>
            </ul>
          </div>
        </div>
        <div>
          <h1>Contacts</h1>
          <p>
            You can contact developers via mail:
            <ul>
              <li>
                <a href="mailto:a.gorb@innopolis.university">
                  a.gorb@innopolis.university
                </a>
              </li>
              <li>
                <a href="mailto:m.ivanova@innopolis.university">
                  m.ivanova@innopolis.university
                </a>
              </li>
              <li>
                <a href="mailto:y.chukanova@innopolis.university">
                  y.chukanova@innopolis.university
                </a>
              </li>
            </ul>
            or via telegram:
            <ul>
              <li>
                <a href="https://t.me/AnutkaGorb">@AnutkaGorb</a>
              </li>
              <li>
                <a href="https://t.me/mari1647iv">@mari1647iv</a>
              </li>
              <li>
                <a href="https://t.me/julia953197">@julia953197</a>
              </li>
            </ul>
          </p>
          <p></p>
        </div>
      </main>
      <footer id="contacts" className={style.footer}>
        <div>
          <p>email: some@email</p>
          <p>phone: +79876543210</p>
        </div>
        <div className={style.media}>
          <i>
            <GitHub />
          </i>
          <i>
            <Figma />
          </i>
          <i>
            <Instagram />
          </i>
          <i>
            <Facebook />
          </i>
          <i>
            <Youtube />
          </i>
          <i>
            <Mail />
          </i>
        </div>
        <div className={style.contacts}>
          <p>© 2021 «MusicLearn»</p>
          <p>Developed by MusicLearn team, Innopolis University</p>
        </div>
      </footer>
    </div>
  );
}

export default ContactsPage;
