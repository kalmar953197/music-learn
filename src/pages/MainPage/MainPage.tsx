import React, { Component } from "react";

import style from "./MainPage.css";

import HomeLink from "../../components/HomeLink/HomeLink";
import MainTitle from "../../components/MainTitle/MainTitle";
import ProfileButton from "../../components/ProfileButton/ProfileButton";
import CourseCard from "../../components/CourseCard/CourseCard";

import { GitHub, Figma, Youtube, Mail, Edit } from "react-feather";
import Teacher from "../../../assets/images/teacher.png";
import Button from "../../components/Button/Button";
import { CourseIMG_eng, NotesIMG } from "../../../assets/images";

import { useSelector } from "react-redux";
import { authorizationSelectors } from "../../__data__/features/authorization/authorizationSlice";

function MainPage(): JSX.Element {
  const is_auth = useSelector(authorizationSelectors.isAuthorized);
  console.log(is_auth);
  return (
    <body className={style.body}>
      <header className={style.header}>
        <div className={style.navigation}>
          <HomeLink />
          <div className={style.links}>
            <Button
              color="white"
              width="auto"
              height="auto"
              href="/music-learn/courses"
              text="Courses"
              type="underlined"
            />
            <Button
              color="white"
              width="auto"
              height="auto"
              href="/"
              text="Notes"
              type="underlined"
            />
            <Button
              color="white"
              width="auto"
              height="auto"
              href="/music-learn/contacts"
              text="Contacts"
              type="underlined"
            />
            <ProfileButton isAuthorized={is_auth} />
          </div>
        </div>
        <div role="banner">
          <MainTitle />
        </div>
      </header>
      <div className={style.our_vision}>
        <h1 className={style.vision_title}> Our vision</h1>
        <div className={style.vision_text}>
          Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
          sint. Velit officia consequat duis enim velit mollit. Exercitation
          veniam consequat sunt nostrud amet.
        </div>
      </div>

      <div className={style.author_info}>
        <div className={style.author_text}>
          <h1 className={style.author_title}>
            <Edit size={45} /> Author
          </h1>
          <div className={style.about_author}>
            Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
            sint. Velit officia consequat duis enim velit mollit. Exercitation
            veniam consequat sunt nostrud amet.Amet minim mollit non deserunt
            ullamco est sit aliqua dolor do amet sint. Velit officia consequat
            duis enim velit mollit. Exercitation veniam consequat sunt nostrud
            amet.Amet minim mollit non deserunt ullamco est sit aliqua dolor do
            amet sint. Velit officia consequat duis enim velit mollit.
            Exercitation veniam consequat sunt nostrud amet.Amet minim mollit
            non deserunt ullamco est sit aliqua dolor do amet sint. Velit
            officia consequat duis enim velit mollit. Exercitation veniam
            consequat sunt nostrud amet.
          </div>
        </div>
        <img
          src={Teacher}
          alt="Author's picture"
          className={style.author_photo}
        />
      </div>

      <div className={style.categories}>
        <h1 className={style.get_started}> Get Started with MusicLearn</h1>
        <div className={style.categories_fast_links}>
          <CourseCard
            is_category={true}
            href="/music-learn/courses"
            text="Courses"
            image={CourseIMG_eng}
            description="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit."
          />
          <CourseCard
            is_category={true}
            href="/music-learn/notes"
            text="Notes"
            image={NotesIMG}
            description="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit."
          />
        </div>
      </div>
      <footer id="contacts" className={style.footer}>
        <div>
          <p>email: some@email</p>
          <p>phone: +79876543210</p>
        </div>
        <div className={style.media}>
          <a href="https://bitbucket.org/kalmar953197/music-learn/src/master/">
            <GitHub />
          </a>
          <a href="https://www.figma.com/file/82iw0bNHKEGw9HONbVq6Y5/music-learn?node-id=0%3A1">
            <Figma />
          </a>
          <a href="https://www.youtube.com/">
            <Youtube />
          </a>
          <a href="/music-learn/contacts">
            <Mail />
          </a>
        </div>
        <div className={style.contacts}>
          <p>© 2021 «MusicLearn»</p>
          <p>Developed by MusicLearn team, Innopolis University</p>
        </div>
      </footer>
    </body>
  );
}

export default MainPage;
