import React, { Component } from "react";

import style from "./MyCoursesPage.css";

import HomeLink from "../../components/HomeLink/HomeLink";
import MainTitle from "../../components/MainTitle/MainTitle";
import ProfileButton from "../../components/ProfileButton/ProfileButton";
import CourseCard from "../../components/CourseCard/CourseCard";
import { CourseIMG_eng } from "../../../assets/images";

import {
  GitHub,
  Figma,
  Instagram,
  Facebook,
  Youtube,
  Mail,
} from "react-feather";

import Button from "../../components/Button/Button";

import { useSelector } from "react-redux";
import { authorizationSelectors } from "../../__data__/features/authorization/authorizationSlice";

import mlAxios from "../../utils/axios";
class MyCoursesPage extends Component {
  courses = [];

  componentDidMount(): void {
    this.loadedCourses();
    // console.log(this.courses);
  }

  loadedCourses = async (): Promise<void> => {
    try {
      const response = await mlAxios("/user/courses", {
        method: "GET",
      });
      this.courses = response.data;
      this.forceUpdate();
    } catch (err) {
      console.log(err);
    }
  };

  render(): JSX.Element {
    return (
      <div className={style.page}>
        <div className={style.sidebar}>
          <div className={style.menu}>
            <div className={style.chosen}>
              <Button
                color="white"
                width="250px"
                height="65px"
                href="/music-learn/my-courses"
                text="My Courses"
                type="simple"
              />
            </div>
            <Button
              color="white"
              width="250px"
              height="65px"
              href="/music-learn/"
              text="My Notes"
              type="simple"
            />
            <Button
              color="white"
              width="250px"
              height="65px"
              href="/music-learn/"
              text="Settings"
              type="simple"
            />
          </div>
          <Button
            color="pink"
            width="250px"
            height="65px"
            href="/music-learn/login"
            text="Log out"
            type="focused"
          />
        </div>
        <div className={style.body}>
          <header className={style.header}>
            <div className={style.navigation}>
              <HomeLink />
              <div className={style.links}>
                <Button
                  color="white"
                  width="auto"
                  height="auto"
                  href="/music-learn/courses"
                  text="Courses"
                  type="underlined"
                />
                <Button
                  color="white"
                  width="auto"
                  height="auto"
                  href="/"
                  text="Notes"
                  type="underlined"
                />
                <Button
                  color="white"
                  width="auto"
                  height="auto"
                  href="/music-learn/contacts"
                  text="Contacts"
                  type="underlined"
                />
                <ProfileButton isAuthorized={true} />
              </div>
            </div>
            <div role="banner">
              <MainTitle />
            </div>
          </header>
          <main className={style.main}>
            {this.courses?.map((course) => (
              <CourseCard
                key={course.id}
                is_category={false}
                href={course.href}
                text={course.name}
                image={CourseIMG_eng}
              />
            ))}
          </main>
          <footer id="contacts" className={style.footer}>
            <div>
              <p>email: some@email</p>
              <p>phone: +79876543210</p>
            </div>
            <div className={style.media}>
              <i>
                <GitHub />
              </i>
              <i>
                <Figma />
              </i>
              <i>
                <Instagram />
              </i>
              <i>
                <Facebook />
              </i>
              <i>
                <Youtube />
              </i>
              <i>
                <Mail />
              </i>
            </div>
            <div className={style.contacts}>
              <p>© 2021 «MusicLearn»</p>
              <p>Developed by MusicLearn team, Innopolis University</p>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

export default MyCoursesPage;
