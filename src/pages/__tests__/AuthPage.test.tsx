import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'

import AuthPage from "../AuthPage/AuthPage";


describe('<AuthPage />', () => {
    it('rendering', () => {
        const wrapper = mount(<AuthPage />);

        expect(wrapper).toMatchSnapshot();
    });
});