import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'

import ContactsPage from "../ContactsPage/ContactsPage";
import {Provider} from "react-redux";
import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {authorizationReducer} from "../../__data__/features/authorization/authorizationSlice";

const store = configureStore({
    reducer: combineReducers({
        authorization: authorizationReducer,
    }),
});

describe('<ContactsPage />', () => {
    it('rendering', () => {
        const wrapper = mount(
            <Provider store={store}>
                <ContactsPage />
            </Provider>

        );

        expect(wrapper).toMatchSnapshot();
    });
});

