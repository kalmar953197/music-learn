import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'

import CoursesPage  from '../CoursesPage/CoursesPage';


describe('<CoursePage />', () => {
    it('rendering', () => {
        const wrapper = mount(<CoursesPage />);

        expect(wrapper).toMatchSnapshot();
    });
});