import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'

import {Provider} from "react-redux";
import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {authorizationReducer} from "../../__data__/features/authorization/authorizationSlice";
import MainPage from "../MainPage/MainPage";

const store = configureStore({
    reducer: combineReducers({
        authorization: authorizationReducer,
    }),
});

describe('<MainPage />', () => {
    it('rendering', () => {
        const wrapper = mount(
            <Provider store={store}>
                <MainPage />
            </Provider>

        );

        expect(wrapper).toMatchSnapshot();
    });
});

