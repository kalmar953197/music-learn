import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals'
import MyCoursesPage from "../MyCoursesPage/MyCoursesPage";



describe('<MyCoursePage />', () => {
    it('rendering', () => {
        const wrapper = mount(<MyCoursesPage />);

        expect(wrapper).toMatchSnapshot();
    });
});