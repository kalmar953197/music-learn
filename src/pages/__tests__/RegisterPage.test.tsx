import * as React from 'react'
import { mount } from 'enzyme';

import { describe, it, expect, jest } from '@jest/globals'

import {Provider} from "react-redux";
import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {authorizationReducer} from "../../__data__/features/authorization/authorizationSlice";
import RegisterPage from "../AuthPage/RegisterPage";

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom') as any,
    useHistory: jest.fn(),
}));

const store = configureStore({
    reducer: combineReducers({
        authorization: authorizationReducer,
    }),
});

describe('<RegisterPage />', () => {
    it('rendering', () => {
        const wrapper = mount(
            <Provider store={store}>
                <RegisterPage />
            </Provider>

        );

        expect(wrapper).toMatchSnapshot();
    });
});

