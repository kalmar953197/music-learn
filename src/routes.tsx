import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { URLs } from "./urls";

import MyCoursesPage from "./pages/MyCoursesPage/MyCoursesPage";
import RegisterPage from "./pages/AuthPage/RegisterPage";
import SignInPage from "./pages/AuthPage/SignInPage";
import CoursesPage from "./pages/CoursesPage/CoursesPage";
import ContactsPage from "./pages/ContactsPage/ContactsPage";
import MainPage from "./pages/MainPage/MainPage";

export const useRoutes = (isAuthenticated) => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path={URLs.myCourses.url} component={MyCoursesPage} />
        <Route path={URLs.courses.url} component={CoursesPage} />
      </Switch>
    );
  }

  return (
    <Switch>
      <Route path={URLs.login.url}>
        <SignInPage />
      </Route>
      <Route path={URLs.register.url}>
        <RegisterPage />
      </Route>
      <Route path={URLs.myCourses.url}>
        <MyCoursesPage />
      </Route>
      <Route path={URLs.courses.url}>
        <CoursesPage />
      </Route>
      <Route path={URLs.contacts.url}>
        <ContactsPage />
      </Route>
      <Route path={URLs.home.url} exact>
        <MainPage />
      </Route>

      <Route path="*">
        <Redirect to={URLs.home.url} />
      </Route>
    </Switch>
  );
};
