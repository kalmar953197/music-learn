import { getNavigations } from "@ijl/cli";

const navigations = getNavigations("music-learn");

export const baseUrl = navigations["music-learn"];

export const URLs = {
  home: {
    url: navigations["music-learn"],
  },
  login: {
    url: navigations["music-learn.login"],
  },
  register: {
    url: navigations["music-learn.register"],
  },
  myCourses: {
    url: navigations["music-learn.my-courses"],
  },
  courses: {
    url: navigations["music-learn.courses"],
  },
  contacts: {
    url: navigations["music-learn.contacts"],
  },
};
