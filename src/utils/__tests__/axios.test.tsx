import * as React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import { baseApiUrl } from "../axios";

describe("baseApiUrl", () => {
  it("stores api path", () => {
    expect(baseApiUrl).toBe("/api/" || "/multystub/music-learn/");
  });
});
