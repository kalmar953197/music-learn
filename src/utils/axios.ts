import axios from "axios";
import { getConfigValue } from "@ijl/cli";

export const baseApiUrl = getConfigValue("music-learn.api");
const mlAxios = axios.create({
  baseURL: baseApiUrl,
  headers: {
    "Content-Type": "application/json;charset=utf-8",
  },
});
export default mlAxios;
