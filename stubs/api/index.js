/* eslint-disable global-require */
/* eslint-disable @typescript-eslint/no-var-requires */
const router = require('express').Router();

router.get('/courses', (req, res) => {
  res.send(require('./mocks/courses/success.json'));
});

router.get('/courses/1', (req, res) => {
  res.send(require('./mocks/courses/1/success.json'));
});

router.get('/notes', (req, res) => {
  res.send(require('./mocks/notes/success.json'));
});

router.get('/user/courses', (req, res) => {
  res.send(require('./mocks/user/courses/success.json'));
});

router.post('/user/courses', (req, res) => {
  res.send(require('./mocks/user/courses/add/success.json'));
});

router.get('/user/notes', (req, res) => {
  res.send(require('./mocks/user/notes/success.json'));
});

router.get('/user', (req, res) => {
  res.send(require('./mocks/user/success.json'));
});

router.post('/user', (req, res) => {
  res.send(require('./mocks/user/success.json'));
});

router.post('/login', (req, res) => {
  if (req.body.login === 'error' || req.body.password === 'error') {
    return res.status(401).send({ message: 'Unauthorized Error' });
  }

  res.send(require('./mocks/user/login/success.json'));
  return null;
});

router.get('/logout', (req, res) => {
  res.send(require('./mocks/user/logout/success.json'));
});
/* eslint-enable global-require */

module.exports = router;
